class InvalidGenreError(TypeError):
    """
    InvalidGenreError exception is raised once an unknown genre
    is used.
    """
    pass
