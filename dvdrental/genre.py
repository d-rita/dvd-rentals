class Genre:
    def __init__(self, name, description):
        self.name = name
        self.description = description


class Romance(Genre):
    """Notebook <> Hallmark <> Mills & Boone"""

    def __init__(self):
        super().__init__(self.__class__.__name__, 'PG16+')


class Horror(Genre):
    """ Jeepers Creepers <> Saw <> Wrong Turn """

    def __init__(self):
        super().__init__(
            self.__class__.__name__,
            'Litmus test for cowardice'
        )


class Adventure(Genre):
    """Indiana Jones <> Jack Sparrow's Pirates of the carribean"""

    def __init__(self):
        super().__init__(
            self.__class__.__name__,
            'Long walks in the park'
        )


class Action(Genre):
    """Olympus Has Fallen <> Taken <> Mission Impossible 6: Fallout"""

    def __init__(self):
        super().__init__(
            self.__class__.__name__,
            'Maaso kulutimbe'
        )


class Comedy(Genre):
    """ Represents comedy movies"""

    def __init__(self):
        super().__init__(
            self.__class__.__name__,
            'Laughter is for the heart'
        )
