class Movie:
    def __init__(self, title, duration, genre, year, actors):
        self.title = title
        self.duration = duration
        self.year = year
        self.genre = genre
        self.actors = actors
