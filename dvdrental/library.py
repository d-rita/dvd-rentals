from dvdrental.exceptions import InvalidGenreError
from dvdrental.genre import Genre
from dvdrental.movie import Movie


class MovieLibrary:
    """
    Represents a DVD Rental store and all operations that
    the owner can carry out.
    """

    def __init__(self):
        self.movies = dict()
        self.movies_count = dict()

    def add_movie(self, title, duration, genre, year, actors=[]):
        if not isinstance(genre, Genre):
            raise InvalidGenreError("invalid genre: given genre is unknown")

        title = title.lower()

        if title in self.movies:
            self.movies_count[title] += 1
        else:
            self.movies_count[title] = 1
            self.movies[title] = Movie(title, duration, genre, year, actors)
        return self.movies

    def edit_movie(self,
                   title, duration=None, genre=None, year=None, actors=None):

        title = title.lower()

        if title not in self.movies:
            return False

        movie = self.movies[title]
        movie.duration = duration or movie.duration
        movie.genre = genre or movie.genre
        movie.year = year or movie.year
        movie.actors = actors or movie.actors

        return True

    def delete_movie_entry(self, title):
        title = title.lower()

        if title not in self.movies:
            return False

        del self.movies[title]
        del self.movies_count[title]

        return True

    def all_movie_titles(self):
        return list(self.movies.keys())

    def rent_out_movie(self, title):
        title = title.lower()

        if title not in self.movies_count or self.movies_count[title] < 1:
            return False

        self.movies_count[title] -= 1
        return True

    def movies_with_genre(self, genre):
        if not isinstance(genre, Genre):
            raise InvalidGenreError('unknown genre')

        return [mv.title for mv in self.movies.values() if mv.genre == genre]

    def get_movie_with_title(self, title):
        return self.movies.get(title.lower())
