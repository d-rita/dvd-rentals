import unittest
from dvdrental.library import MovieLibrary
from dvdrental.movie import Movie
from dvdrental.genre import Action, Romance
from dvdrental.exceptions import InvalidGenreError


class TestMovieLibrary(unittest.TestCase):
    def setUp(self):
        self.movie_lib = MovieLibrary()
        self.sample_movie = dict(
            title='Mission Impossible 6: Fall out',
            duration='148min',
            year=2018,
            genre=Action(),
            actors=[
                'Tom Cruise',
                'Henry Cavill',
                'Rebecca Ferguson',
                'Angela Bassett'
            ]
        )

        self.bad_genre = dict(
            title='Mission Impossible 6: Fall out',
            duration='148min',
            year=2018,
            genre='String_GENRE',
            actors=[
                'None'
            ]
        )

        self.no_actors = dict(
            title='Mission Impossible 6: Fall out',
            duration='148min',
            year=2018,
            genre=Action()
        )

    def test_add_movie_is_successful(self):

        self.assertEqual(len(self.movie_lib.movies), 0)
        self.movie_lib.add_movie(**self.sample_movie)
        print(self.movie_lib.all_movie_titles())
        self.assertIn(self.sample_movie["title"].lower(),
                      self.movie_lib.all_movie_titles())
        self.assertEqual(len(self.movie_lib.movies), 1)

    def test_add_increments_for_existing_movie(self):
        self.assertEqual(len(self.movie_lib.movies), 0)
        self.movie_lib.add_movie(**self.sample_movie)
        self.assertEqual(len(self.movie_lib.movies), 1)

        self.movie_lib.add_movie(**self.sample_movie)
        self.assertEqual(len(self.movie_lib.movies), 1)
        self.assertNotEqual(len(self.movie_lib.movies), 2)
        title = self.sample_movie["title"]
        self.assertEqual(self.movie_lib.movies_count[title.lower()], 2)

    def test_add_fails_for_invalid_genre(self):
        with self.assertRaises(InvalidGenreError):
            self.movie_lib.add_movie(**self.bad_genre)
        self.assertEqual(len(self.movie_lib.movies), 0)

    def test_can_add_with_no_actors(self):
        self.assertEqual(len(self.movie_lib.movies), 0)
        self.movie_lib.add_movie(**self.no_actors)
        self.assertEqual(len(self.movie_lib.movies), 1)
        self.assertIn(self.no_actors["title"].lower(),
                      self.movie_lib.all_movie_titles()
                      )

    def test_can_edit_movie(self):
        self.movie_lib.add_movie(**self.no_actors)
        is_edited = self.movie_lib.edit_movie(
            self.no_actors["title"].lower(),
            genre=Romance(),
            actors=[
                'Henry Cavill',
                'Rebecca Ferguson',
                'Angela Bassett'
            ]
        )
        self.assertTrue(is_edited)

        movie = self.movie_lib.get_movie_with_title(
            self.no_actors["title"].lower()
        )
        self.assertIsInstance(movie.genre, Romance)
        self.assertEqual(len(movie.actors), 3)

    def test_edit_fails_for_non_existent_title(self):
        self.assertFalse(self.movie_lib.edit_movie("non-existent", "2min"))

    def test_can_delete_movie(self):
        self.movie_lib.add_movie(**self.no_actors)
        title = self.no_actors["title"].lower()
        self.assertTrue(self.movie_lib.delete_movie_entry(title))

    def test_delete_fails_for_non_existent_title(self):
        self.assertFalse(
            self.movie_lib.delete_movie_entry("non existent title")
        )

    def test_all_movie_titles(self):
        self.assertEqual(len(self.movie_lib.all_movie_titles()), 0)
        self.movie_lib.add_movie(**self.no_actors)
        self.assertEqual(len(self.movie_lib.all_movie_titles()), 1)

    def test_get_movie_title(self):
        self.assertIsNone(self.movie_lib.get_movie_with_title("no title"))
        self.movie_lib.add_movie(**self.no_actors)
        self.assertIsInstance(
            self.movie_lib.get_movie_with_title(
                "Mission Impossible 6: Fall out"
            ),
            Movie
        )

    def test_can_rent_out_movie(self):
        self.assertFalse(self.movie_lib.rent_out_movie("not existing"))
        self.movie_lib.add_movie(**self.sample_movie)
        self.assertTrue(
            self.movie_lib.rent_out_movie("Mission Impossible 6: Fall out")
        )

    def test_cannot_rent_out_of_stock_movie(self):
        self.movie_lib.add_movie(**self.sample_movie)
        self.movie_lib.rent_out_movie("Mission Impossible 6: Fall out")
        self.movie_lib.rent_out_movie("Mission Impossible 6: Fall out")
        self.assertFalse(
            self.movie_lib.rent_out_movie(
                "Mission Impossible 6: Fall out")
        )

    def test_can_delete_movie_entry(self):
        self.movie_lib.add_movie(**self.sample_movie)
        self.assertTrue(
            self.movie_lib.delete_movie_entry(
                self.sample_movie['title'].lower()
            )
        )
        self.assertNotIn(
            self.sample_movie["title"].lower(), self.movie_lib.movies
        )

        self.assertNotIn(
            self.sample_movie["title"].lower(), self.movie_lib.movies_count
        )

    def test_cannot_delete_non_existent_movie(self):
        self.assertFalse(
            self.movie_lib.delete_movie_entry(
                "non existent"
            )
        )

    def tearDown(self):
        self.movie_lib = None
